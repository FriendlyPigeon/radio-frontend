-- :name create-user! :! :n
-- :doc creates a new user record
INSERT INTO users
(id, first_name, last_name, email, pass)
VALUES (:id, :first_name, :last_name, :email, :pass)

-- :name update-user! :! :n
-- :doc updates an existing user record
UPDATE users
SET first_name = :first_name, last_name = :last_name, email = :email
WHERE id = :id

-- :name get-user :? :1
-- :doc retrieves a user record given the id
SELECT * FROM users
WHERE id = :id

-- :name delete-user! :! :n
-- :doc deletes a user record given the id
DELETE FROM users
WHERE id = :id


-- :name song-liked-by-user* :? :1
-- :doc retrieves 1 if song liked by user otherwise 0
SELECT COUNT(*) AS liked FROM likes
WHERE user = :user AND filename = :filename


-- :name like-song! :! :n
-- :doc insert like for given user and filename
INSERT INTO likes
(user, filename)
VALUES (:user, :filename)

-- :name unlike-song! :! :n
-- :doc delete like for given user and filename
DELETE FROM likes
WHERE user = :user AND filename = :filename

-- :name songs-liked-by-user* :? :*
-- :doc retrieves all songs liked by given user
SELECT filename FROM likes
WHERE user = :user

-- :name most-liked-songs* :? :*
-- :doc retrieves all songs liked ordered by most liked
SELECT filename, COUNT(user) AS likes FROM likes
GROUP BY filename
ORDER BY COUNT(user) DESC
