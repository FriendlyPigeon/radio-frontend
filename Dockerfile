FROM openjdk:8-alpine

COPY target/uberjar/radio_frontend.jar /radio_frontend/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/radio_frontend/app.jar"]
