(ns radio-frontend.core
  (:require
    [reagent.core :as r]
    [reagent.dom :as rdom]
    [goog.events :as events]
    [goog.history.EventType :as HistoryEventType]
    [markdown.core :refer [md->html]]
    [radio-frontend.ajax :as ajax]
    [ajax.core :refer [GET POST]]
    [reitit.core :as reitit]
    [clojure.string :as string]
    [radio-frontend.websockets :as ws])
  (:import goog.History))

(defonce session (r/atom {:page :home}))
(defonce logged-in? (r/atom false))
(defonce current-song (r/atom ""))
(defonce song-liked (r/atom false))
(defonce songs-liked (r/atom []))
(defonce queue (r/atom []))
(defonce all-songs (r/atom []))
(defonce my-like-modal-shown? (r/atom false))
(defonce all-songs-modal-shown? (r/atom false))

(defn nav-link [uri title page]
  [:a.navbar-item
   {:href   uri
    :class (when (= page (:page @session)) "is-active")}
   title])

(defn navbar [] 
  (r/with-let [expanded? (r/atom false)]
    [:nav.navbar.is-info>div.container
     [:div.navbar-brand
      [:a.navbar-item {:href "/" :style {:font-weight :bold}} "radio_frontend"]
      [:span.navbar-burger.burger
       {:data-target :nav-menu
        :on-click #(swap! expanded? not)
        :class (when @expanded? :is-active)}
       [:span][:span][:span]]]
     [:div#nav-menu.navbar-menu
      {:class (when @expanded? :is-active)}
      [:div.navbar-start
       [nav-link "#/" "Home" :home]
       [nav-link "#/about" "About" :about]
       (if @logged-in?
         [nav-link "/oauth2/discord" "Logged in"]
         [nav-link "/oauth2/discord" "Login with Discord"])
       ]]]))

(defn about-page []
  [:section.section>div.container>div.content
   [:img {:src "/img/warning_clojure.png"}]])

(defonce messages (r/atom []))

(defn receive-ws-message! [msg]
  (if-let [permissions (aget msg "permissions")]
    (if (aget permissions "logged-in")
      (reset! logged-in? true)
      (reset! logged-in? false)))
  (if-let [song (aget msg "song")]
    (reset! current-song song))
  (let [liked (aget msg "liked")]
    (if (not= liked nil)
      (reset! song-liked liked)))
  (if-let [songs (aget msg "songs")]
    (reset! songs-liked songs))
  (if-let [songs (aget msg "all-songs")]
    (reset! all-songs songs))
  (if-let [updated-queue (aget msg "queue")]
    (reset! queue updated-queue)))

(defn my-like-modal []
  [:div {:class (str "modal " (when @my-like-modal-shown? "is-active"))}
   [:div {:class "modal-background"}]
   [:div {:class "modal-card"}
    [:header {:class "modal-card-head"}
     [:p {:class "modal-card-title"} "Liked Songs"]
     [:button {:class "delete" :aria-label "close" :on-click #(reset! my-like-modal-shown? false)}]]
    [:section {:class "modal-card-body"}
     [:table {:class "table"}
      [:thead
       [:tr
        [:th
         [:abbr {:title "Title"} "Title"]]
        [:th
         [:abbr {:title "Play"} "Play"]]]]
      [:tbody
       (for [song @songs-liked]
         ^{:key (aget song "Title")} [:tr
                                      [:td
                                       [:span (aget song "Title")]]
                                      [:td
                                       [:a {:on-click #(ws/send-transit-msg! {:existing-song-queue (aget song "Id")})} "Play"]]])]]]
    [:footer {:class "modal-card-foot"}
     [:button {:class "button" :on-click #(reset! my-like-modal-shown? false)} "Cancel"]]]])

(defn all-songs-modal []
  [:div {:class (str "modal " (when @all-songs-modal-shown? "is-active"))}
   [:div {:class "modal-background"}]
   [:div {:class "modal-card"}
    [:header {:class "modal-card-head"}
     [:p {:class "modal-card-title"} "All Songs"]
     [:button {:class "delete" :aria-label "close" :on-click #(reset! all-songs-modal-shown? false)}]]
    [:section {:class "modal-card-body"}
     [:table {:class "table"}
      [:thead
       [:tr
        [:th
         [:abbr {:title "Title"} "Title"]]
        [:th
         [:abbr {:title "Likes"} "Likes"]]
        [:th
         [:abbr {:title "Play"} "Play"]]]]
      [:tbody
       (for [song @all-songs]
         ^{:key (aget song "Title")} [:tr
                                      [:td
                                       [:span (aget song "Title")]]
                                      [:td
                                       [:span (aget song "likes")]]
                                      [:td
                                       [:a {:on-click #(ws/send-transit-msg! {:existing-song-queue (aget song "Id")})} "Play"]]])]]]
    [:footer {:class "modal-card-foot"}
     [:button {:class "button" :on-click #(reset! all-songs-modal-shown? false)} "Cancel"]]]])  

(defn home-page []
  [:div
   [my-like-modal]
   [all-songs-modal]
   [:div
    [:video {:controls true :autoPlay true :playsInline true}
     [:source {:src "http://209.141.34.128:8000/stream.mp3" :type "audio/mpeg"}]]]
   [:div
    [:span @current-song]]
   [:div
    (for [song @queue]
      ^{:key song} [:div
                    [:span song]])]
   [:div {:style {:display "block"}}
    [:div {:style {:display "flex" :justify-content "space-around"}}
     (case @song-liked
       false [:a {:on-click #(ws/send-transit-msg! {:song-like-toggle true})}
              [:svg {:style {:width "30vw"} :viewBox "0 0 24 24"}
               [:path {:fill "#FFFFFF" :d "M12.1,18.55L12,18.65L11.89,18.55C7.14,14.24 4,11.39 4,8.5C4,6.5 5.5,5 7.5,5C9.04,5 10.54,6 11.07,7.36H12.93C13.46,6 14.96,5 16.5,5C18.5,5 20,6.5 20,8.5C20,11.39 16.86,14.24 12.1,18.55M16.5,3C14.76,3 13.09,3.81 12,5.08C10.91,3.81 9.24,3 7.5,3C4.42,3 2,5.41 2,8.5C2,12.27 5.4,15.36 10.55,20.03L12,21.35L13.45,20.03C18.6,15.36 22,12.27 22,8.5C22,5.41 19.58,3 16.5,3Z"}]]]
       true [:a {:on-click #(ws/send-transit-msg! {:song-like-toggle true})}
             [:svg {:style {:width "30vw"} :viewBox "0 0 24 24"}
              [:path {:fill "#FFFFFF" :d "M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z"}]]])
     [:a {:on-click #(ws/send-transit-msg! {:song-skip true})}
      [:svg {:style {:width "30vw"} :viewBox "0 0 24 24"}
       [:path {:fill "#FFFFFF" :d "M16,18H18V6H16M6,18L14.5,12L6,6V18Z"}]]]]
    [:div {:style {:display "flex" :justify-content "space-around"}}
     [:a {:on-click #(do
                       (ws/send-transit-msg! {:songs-liked true})
                       (reset! my-like-modal-shown? true))}
      [:svg {:style {:width "30vw"} :viewBox "-12 0 192 192"}
       [:path {:fill "#FFFFFF" :d "M163.226,47.876c-4.787-46.347-56.53-45.631-81.462-23.259C56.832,2.246,5.113,1.53,0.313,47.876 c-6.191,59.663,81.45,105.892,81.45,105.892S169.402,107.545,163.226,47.876z M66.638,105.625V57.918l41.316,23.856L66.638,105.625Z"}]]]
     [:a {:on-click #(do
                       (ws/send-transit-msg! {:all-songs true})
                       (reset! all-songs-modal-shown? true))}
      [:svg {:style {:width "30vw"} :viewBox "0 0 24 24"}
       [:path {:fill "#FFFFFF" :d "M16 17V19H2V17S2 13 9 13 16 17 16 17M12.5 7.5A3.5 3.5 0 1 0 9 11A3.5 3.5 0 0 0 12.5 7.5M15.94 13A5.32 5.32 0 0 1 18 17V19H22V17S22 13.37 15.94 13M15 4A3.39 3.39 0 0 0 13.07 4.59A5 5 0 0 1 13.07 10.41A3.39 3.39 0 0 0 15 11A3.5 3.5 0 0 0 15 4Z"}]]]]]])

(def pages
  {:home #'home-page
   :about #'about-page})

(defn page []
  [(pages (:page @session))])

;; -------------------------
;; Routes

(def router
  (reitit/router
    [["/" :home]
     ["/about" :about]]))

(defn match-route [uri]
  (->> (or (not-empty (string/replace uri #"^.*#" "")) "/")
       (reitit/match-by-path router)
       :data
       :name))
;; -------------------------
;; History
;; must be called after routes have been defined
(defn hook-browser-navigation! []
  (doto (History.)
    (events/listen
      HistoryEventType/NAVIGATE
      (fn [^js/Event.token event]
        (swap! session assoc :page (match-route (.-token event)))))
    (.setEnabled true)))

;; -------------------------
;; Initialize app
(defn fetch-docs! []
  (GET "/docs" {:handler #(swap! session assoc :docs %)}))

(defn ^:dev/after-load mount-components []
  (rdom/render [#'navbar] (.getElementById js/document "navbar"))
  (rdom/render [#'page] (.getElementById js/document "app")))

(defn init! []
  (ajax/load-interceptors!)
  (fetch-docs!)
  (hook-browser-navigation!)
  (ws/make-websocket! (str "ws://" (.-host js/location) "/ws") receive-ws-message!)
  (mount-components))
