(ns radio-frontend.handler
  (:require
    [radio-frontend.middleware :as middleware]
    [radio-frontend.layout :refer [error-page]]
    [radio-frontend.routes.home :refer [home-routes]]
    [reitit.ring :as ring]
    [ring.middleware.content-type :refer [wrap-content-type]]
    [ring.middleware.webjars :refer [wrap-webjars]]
    [radio-frontend.env :refer [defaults]]
    [mount.core :as mount]
    [ring.middleware.oauth2 :refer [wrap-oauth2]]))

(mount/defstate init-app
  :start ((or (:init defaults) (fn [])))
  :stop  ((or (:stop defaults) (fn []))))

(mount/defstate app-routes
  :start
   (ring/ring-handler
    (ring/router
     [(home-routes)])
    (ring/routes
     (ring/create-resource-handler
      {:path "/"})
     (wrap-content-type
      (wrap-webjars (constantly nil)))
     (ring/create-default-handler
      {:not-found
       (constantly (error-page {:status 404, :title "404 - Page not found"}))
       :method-not-allowed
       (constantly (error-page {:status 405, :title "405 - Not allowed"}))
       :not-acceptable
       (constantly (error-page {:status 406, :title "406 - Not acceptable"}))}))))

(defn wrap-discord-oauth2 [handler]
  (wrap-oauth2
   handler
   {:discord
    {:authorize-uri "https://discord.com/api/oauth2/authorize"
     :access-token-uri "https://discord.com/api/oauth2/token"
     :client-id (System/getenv "CLIENT_ID")
     :client-secret (System/getenv "CLIENT_SECRET")
     :scopes ["identify" "guilds"]
     :launch-uri "/oauth2/discord"
     :redirect-uri "/oauth2/discord/callback"
     :landing-uri "/"}}))

(defn app []
   (-> #'app-routes
       (middleware/wrap-formats)
       (wrap-discord-oauth2)
       (middleware/wrap-base)))
