(ns radio-frontend.routes.home
  (:require
   [clojure.tools.logging :as log]
   [immutant.web.async :as async]
   [radio-frontend.layout :as layout]
   [clojure.java.io :as io]
   [radio-frontend.middleware :as middleware]
   [ring.util.http-response :as response]
   [mpd-clj.core :as mpd]
   [cheshire.core :as c]
   [clj-http.client :as client]
   [radio-frontend.db.core :as db]
   [clojure.java.shell :refer [sh]]
   [clojure.core.match :refer [match]]
   [clojure.string :as str]))

(defonce channels (atom #{}))
(defonce tokens-to-user-ids (atom {}))
(defonce tokens-to-whitelisted (atom {}))
(defonce current-song (atom nil))
(defonce songs-library (atom []))
(defonce queue (atom []))

(defn user-id-from-channel [channel]
  (if-let [token (-> (async/originating-request channel) :oauth2/access-tokens :discord :token)]
    (do
      (let [user-id (get @tokens-to-user-ids (keyword token))]
        user-id))
    nil))

(defn whitelisted-from-channel [channel]
  (if-let [token (-> (async/originating-request channel) :oauth2/access-tokens :discord :token)]
    (do
      (let [whitelisted (get @tokens-to-whitelisted (keyword token))]
        whitelisted))))

(defn song-string [song-info]
  (str (:Artist song-info) " - " (:Title song-info)))

(defn get-queue []
  (let [new-queue (:out (sh "sort" "-n" "-r" :in (:out (sh "grep" "-v" "^0" :in (:out (sh "mpc" "playlist" "-f" "%prio%: %artist% - %title%"))))))]
    (if (= (str/trim new-queue) "")
      (reset! queue [])
      (reset! queue (str/split new-queue #"\n")))))

(defn send-song-change []
  (doseq [channel @channels]
    (let [user-id (user-id-from-channel channel)]
      (async/send! channel (c/generate-string {:song (song-string @current-song)}))
      (let [liked (db/song-liked-by-user* {:user user-id :filename (:file @current-song)})]
        (case liked
          {:liked 0} (async/send! channel (c/generate-string {:liked false}))
          {:liked 1} (async/send! channel (c/generate-string {:liked true}))))
      (get-queue)
      (async/send! channel (c/generate-string {:queue @queue})))))

(defn get-song-library []
  (let [mpd-server (mpd/client {:host "localhost" :port 6600})
        songs-info (mpd/playlist-info mpd-server)]
    (reset! songs-library songs-info)))

(defn song-change []
  (while true
    (let [mpd-server (mpd/client {:host "localhost" :port 6600})
          song-info (mpd/currentsong mpd-server)]
      (reset! current-song song-info)
      (send-song-change)
      (get-song-library)
      (sh "mpc" "idle" "player"))))

(doto
  (Thread. song-change)
  (.setDaemon true)
  (.start))

(defn database-change []
  (while true
    (sh "mpc" "idle" "database")
    (get-song-library)))

(doto
  (Thread. database-change)
  (.setDaemon true)
  (.start))

(defn get-songs-liked-from-library [liked-songs]
  (for [liked-song liked-songs
        song-info @songs-library
        :when (= (:filename liked-song) (:file song-info))]
    song-info))

(defn get-all-songs-info [liked-songs]
  (sort-by :likes #(compare %2 %1)
           (map (fn [song-info]
                  (let [like-count (:likes (first (filter (fn [liked-song]
                                                            (= (:file song-info) (:filename liked-song)))
                                                          liked-songs)))]
                    (if (= like-count nil)
                      (conj song-info {:likes 0})
                      (conj song-info {:likes like-count}))))
                @songs-library)))

(defn queue-existing-song [song-id]
  (let [mpd-server (mpd/client {:host "localhost" :port 6600})
        next-priority (if (not= @queue [])
                        (- (Integer/parseInt (first (str/split (last @queue) #":"))) 1)
                        255)]
    (mpd/set-priority-id next-priority song-id mpd-server)
    (get-queue)))

(defn handle-message! [channel msg]
  (let [parsed-msg (c/parse-string msg)
        filename (:file @current-song)]    
    (if-let [user-id (user-id-from-channel channel)]
      (do
        (match [parsed-msg]
               [{"get-permissions" true}] (do
                                            (async/send! channel (c/generate-string {:permissions {:logged-in true}})))
               [{"song-like-toggle" true}] (do
                                             (let [liked (db/song-liked-by-user* {:user user-id :filename (:file @current-song)})]
                                               (case liked
                                                 {:liked 0} (do
                                                              (db/like-song! {:user user-id :filename filename})
                                                              (async/send! channel (c/generate-string {:liked true})))
                                                 {:liked 1} (do
                                                              (db/unlike-song! {:user user-id :filename filename})
                                                              (async/send! channel (c/generate-string {:liked false}))))))
               [{"song-skip" true}] (do
                                      (sh "mpc" "next"))
               [{"songs-liked" true}] (do
                                        (let [songs-liked (db/songs-liked-by-user* {:user user-id})
                                              songs-liked-info (get-songs-liked-from-library songs-liked)]
                                          (async/send! channel (c/generate-string {:songs songs-liked-info}))))
               [{"all-songs" true}] (do
                                      (let [most-liked-songs (db/most-liked-songs*)
                                            most-liked-songs-info (get-all-songs-info most-liked-songs)]
                                        (async/send! channel (c/generate-string {:all-songs most-liked-songs-info}))))
               [{"queue" true}] (do
                                  (get-queue)
                                  (async/send! channel (c/generate-string {:queue @queue})))
               :else (do
                       (if (whitelisted-from-channel channel)
                         (match [parsed-msg]
                                [{"existing-song-queue" song-id}] (do
                                                                    (get-queue)
                                                                    (queue-existing-song song-id)
                                                                    (doseq [iter-channel @channels]
                                                                      (async/send! iter-channel (c/generate-string {:queue @queue}))))
                                :else ()))))))))
        
(defn connect! [channel]
  (log/info "channel open")
  (async/send! channel (c/generate-string {:song (song-string @current-song)}))
  (let [user-id (user-id-from-channel channel)]
    (if user-id
      (do
        (async/send! channel (c/generate-string {:permissions {:logged-in true}}))
        (let [liked (db/song-liked-by-user* {:user user-id :filename (:file @current-song)})]
          (case liked
            {:liked 0} (async/send! channel (c/generate-string {:liked false}))
            {:liked 1} (async/send! channel (c/generate-string {:liked true})))))))
  (swap! channels conj channel))

(defn disconnect! [channel {:keys [code reason]}]
  (log/info "close code:" code "reason:" reason)
  (swap! channels #(remove #{channel} %)))

(def websocket-callbacks
  "WebSocket callback functions"
  {:on-open    connect!
   :on-close   disconnect!
   :on-message handle-message!})

(defn ws-handler [request]
  (if-let [token (-> request :oauth2/access-tokens :discord :token)]
    (do
      (let [user-id (-> (client/get "https://discord.com/api/users/@me"
                                     {:headers {"Authorization" [(str "Bearer " token)]}})
                         :body
                         (c/parse-string true)
                         :id)
            guilds (-> (client/get "https://discord.com/api/users/@me/guilds"
                                        {:headers {"Authorization" [(str "Bearer " token)]}})
                            :body
                            (c/parse-string-strict))]
        (swap! tokens-to-user-ids assoc (keyword token) user-id)
        (let [whitelisted-guilds (filter (fn [guild]
                                           (= (get guild "id") (System/getenv "WHITELISTED_SERVER")))
                                         guilds)]
          (if (> (count whitelisted-guilds) 0)
            (swap! tokens-to-whitelisted assoc (keyword token) true))))))
  (async/as-channel request websocket-callbacks))

(defn home-page [request]
  (layout/render request "home.html"))

(defn home-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get home-page}]
   ["/ws" {:get ws-handler}]])
