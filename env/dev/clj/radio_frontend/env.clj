(ns radio-frontend.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [radio-frontend.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[radio_frontend started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[radio_frontend has shut down successfully]=-"))
   :middleware wrap-dev})
